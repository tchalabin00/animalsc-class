#include<iostream>
#include "Dog.h"

using namespace std;

//constructor
Dog::Dog() {
	happy=false;
	cout<<"dog created.."<<endl;
}

void Dog::speak(){
	if(happy){cout<<"dog happy"<<endl;}else{cout<<"dog sad"<<endl;}
}

//destructor
Dog::~Dog() {
	cout<<"dog destroyed"<<endl;
}

