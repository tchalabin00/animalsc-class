
#ifndef DOG_H_
#define DOG_H_

class Dog {
private:
	bool happy;

public:
	Dog();
	void speak();
	~Dog();
};

#endif
