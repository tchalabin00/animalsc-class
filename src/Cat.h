#ifndef CAT_H
#define CAT_H
#include<iostream>
using namespace std;

class Cat {

private:
	bool chappy;
	string cname;

public:
	//constractor
	Cat();

	//constructor with param
	Cat(string name);

	//constructor with this
	Cat(string cname, bool chappy);

	//you can directly initialize constructor here with out define him in the cpp file file
	//exmp Cat(string name):name(name){};

	//setter
	void setName(string name);

	//getter
	string getName();

	//method
	void speak();

	//destructor destroy
	~Cat();
};

#endif

