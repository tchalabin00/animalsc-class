#include<iostream>
#include<sstream> //string stream lib
#include "Cat.h"
#include "Dog.h"

using namespace std;

int main() {
	cout << "program start..." << endl;

	Cat cat;	//cat start here
	cat.setName("tom");
	cout << "my name is: " << cat.getName() << endl;
	cat.speak();

	{
		cout << "\n*** constructor with this ***" << endl;
		Cat cat1("this", false);
		cout << "my name is : " << cat1.getName() << endl;
		cat1.speak();
		cout << "Display memory adress of the object" << endl;
		cout << "physical memory adress of object cat1 : " << &cat1 << endl;
	}

	{
		cout << "\n*** constructor with param ***" << endl;
		Cat cat1("lolla");
		cout << "my name is : " << cat1.getName() << endl;
		cat1.speak();
	}

	{//execute only between {} like a mini program in the main program / dog start here
		cout << "\n*** blobk example or mini program***" << endl;
		Dog bob;
		bob.speak();
	}					// dog destroyed here

	//concatination string with int or somthing else
	cout << "\n*** string stream example concat int+string ***" << endl;
	int age = 25;
	string name = "kamara";
	stringstream ss;
	ss << "my name is: " << name << " | " << "my age is: " << age;
	string info = ss.str();
	cout << info << endl;

	cout << "program end..." << endl;
	return 0;		//cat destroyed here
}

