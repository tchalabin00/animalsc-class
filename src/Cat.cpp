#include <iostream>
#include "Cat.h"

using namespace std;

//constractor def
Cat::Cat() {
	chappy = true;
	cout << "cat created" << endl;
}

Cat::Cat(string name) {
	cname = name;
	chappy = false;
}

//constructor with this /on use this because the local variable and the param variable has the same name
Cat::Cat(string cname, bool chappy) {
	this->cname = cname;
	this->chappy = chappy;
}

/* //other initialization mehod
 Cat::Cat(string cname):cname(cname){

 }*/

void Cat::setName(string name) {
	cname = name;
}

string Cat::getName() {
	return cname;
}

void Cat::speak() {
	if (chappy) {
		cout << "im happy" << endl;
	} else {
		cout << "im saaad" << endl;
	}
}

//destructor destroy all variable when program finish
Cat::~Cat() {
	cout << "cat destroyed ..." << endl;
}
